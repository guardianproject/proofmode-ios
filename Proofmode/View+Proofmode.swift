//
//  View+Proofmode.swift
//  Proofmode
//
//  Created by N-Pex on 2023-08-30.
//

import SwiftUI

extension View {
    @ViewBuilder func `if`<Content: View>(_ condition: Bool, transform: (Self) -> Content) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }
}
