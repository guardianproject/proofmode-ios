//
//  DigitalSignaturesView.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-17.
//

import SwiftUI

struct DigitalSignaturesView: View {
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading, spacing: 0) {
                    Text("When you share proof, you will see digital signatures and your public key attached as “.asc” files.")
                    .font(.custom(Font.titleFont, size: 21))
                    .fontWeight(.bold)
                    .foregroundColor(Color("colorDarkGray"))
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding()
                    
                    Text("Here is some more information on what those are.")
                    .font(.custom(Font.bodyFont, size: 13))
                    .fontWeight(.regular)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding()

                    Text("What is my digital signature and public key?")
                    .font(Font.custom(Font.titleFont, size: 13).smallCaps())
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.center)
                    .padding()

                    Text("Every time you install ProofMode, a unique digital identity, consisting of secret and public PGP keys is created. This is powered by the OpenPGP standard. It is used to cryptographically sign your media and supporting proof files. If the PGP signatures match the digital identity (PGP key) of your current ProofMode instance, then the signature was created by that instance of ProofMode.")
                    .font(.custom(Font.bodyFont, size: 13))
                    .fontWeight(.regular)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.leading)
                    .padding()
                    
                    Text("What do I do with it?")
                    .font(Font.custom(Font.titleFont, size: 13).smallCaps())
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.leading)
                    .padding()
                    
                    Text("A public version of your PGP key will be shared along with your media and proof.csv files. This allows a third party to verify the PGP “.asc” signature files that come with the proof data. Including your public PGP key with the proof ensures that it will be preserved in case your device is compromised or you reinstall the app. Remember each time you re-install the app, it will create a new digital identity (PGP key).")
                    .font(.custom(Font.bodyFont, size: 13))
                    .fontWeight(.regular)
                    .foregroundColor(.black)
                    .multilineTextAlignment(.leading)
                    .padding()
                }
            }
            .content
            .frame(height: geometry.size.height, alignment: .top)
        }
    
        .navigationBarTitle("Digital Signatures", displayMode: .inline)
    }
}

struct DigitalSignaturesView_Previews: PreviewProvider {
    static var previews: some View {
        DigitalSignaturesView()
    }
}
