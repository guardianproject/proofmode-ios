//
//  CameraItemPicker.swift
//  Proofmode
//
//  Created by N-Pex on 2023-01-20.
//

import SwiftUI
import Photos

struct CameraItemView: View {
    let size: Double
    @ObservedObject var cameraItem: CameraItem
    
    var body: some View {
        ZStack(alignment: .topTrailing) {
//            if let thumbnailImage = cameraItem.thumbnailImage {
//                Image(uiImage: thumbnailImage).resizable().scaledToFill().clipped()
//            } else {
                ProgressView()
//            }
        }
        .frame(width: size, height: size)
        .cornerRadius(8) /// make the background rounded
//        .overlay( /// apply a rounded border
//            RoundedRectangle(cornerRadius: 8)
//                .stroke(cameraItem.selected ? Color.blue : Color.gray, lineWidth: 3)
//        )
//        .onTapGesture {
//            cameraItem.selected = !cameraItem.selected
//        }
        .onAppear {
//            if cameraItem.thumbnailImage == nil {
//                do {
//                    if let url = cameraItem.url {
//                        let asset = AVURLAsset(url: url, options: nil)
//                        let imgGenerator = AVAssetImageGenerator(asset: asset)
//                        imgGenerator.appliesPreferredTrackTransform = true
//                        let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
//                        cameraItem.thumbnailImage = UIImage(cgImage: cgImage)
//                    } else if let imageData = cameraItem.data {
//                        cameraItem.thumbnailImage = UIImage(data: imageData)
//                    }
//                } catch let error {
//                    print("*** Error generating thumbnail: \(error.localizedDescription)")
//                }
//            }
        }
    }
}

struct CameraItemView_Previews: PreviewProvider {
    static var previews: some View {
        CameraItemView(size: 80, cameraItem: CameraItem(nil, mediaData: UIImage(named: "ill_tut01")!.pngData()!))
    }
}

struct CameraItemPicker: View {
    var onDone: (_ selectedItems:[CameraItem]) -> Void
    var onCancel: () -> Void

    @EnvironmentObject<CameraItems> var cameraItems: CameraItems
        
    private static let initialColumns = 3
    @State private var gridColumns = Array(repeating: GridItem(.flexible()), count: initialColumns)
    @ObservedObject private var items: ObservedCameraItems = ObservedCameraItems()
    
    var body: some View {
        ScrollView {
            // 4. Populate into grid
            LazyVGrid(columns: gridColumns, spacing: 8) {
                ForEach(items.cameraItems) { item in
                    GeometryReader { geo in
                        //                                                NavigationLink(destination: DetailView(item: item)) {
                        CameraItemView(size: geo.size.width, cameraItem: item)
                        //                                               }
                    }
                    .aspectRatio(1, contentMode: .fit)
                }
            }
            .padding()
        }
        .navigationBarBackButtonHidden()
//        .navigationBarTitle("Share \(items.cameraItems.filter({$0.selected}).count) items")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar(content: {
            ToolbarItem(placement: ToolbarItemPlacement.cancellationAction) {
                Button(action: {
                    withAnimation {
                        onCancel()
                    }
                }) {
                    Text("Cancel")
                }
            }
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing) {
//                Button(action: {
//                    withAnimation {
//                        onDone(self.items.cameraItems.filter({$0.selected}))
//                    }
//                }) {
//                    Text("OK")
//                }
//                .disabled(!(self.items.cameraItems.contains(where: { $0.selected })))
            }
        })
        .onAppear(perform: {
            self.items.setItems(cameraItems: self.cameraItems.cameraItems)
        })
    }
}


