//
//  C2PAHelper.swift
//  Proofmode
//
//  Created by N-Pex on 2023-11-17.
//

import SimpleC2PA
import Foundation
import CryptoKit
import LibProofMode

open class C2PAHelper: NSObject {
    public static let shared = C2PAHelper()
    
    private static let CERT_ORGANIZATION = "ProofMode-Root"
    private static let CERT_VALIDITY_DAYS = UInt32(365)
    private static let CERT_ORGANIZATION_USER = "ProofMode-User"
    private static let APP_ICON_URI = "https://proofmode.org/images/avatar.jpg"
    
    func getDocumentsDirectory() -> URL {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.witness.proofmode.ios")
         ?? FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    private func getCertificates() -> (rootCert: Certificate, userCert: Certificate)? {
        let documentsURL = getDocumentsDirectory()

        let certPath = documentsURL.appendingPathComponent("cr.cert")
        let keyPath = documentsURL.appendingPathComponent("cr.key")
        let parentCertPath = documentsURL.appendingPathComponent("crp.cert")
        let parentKeyPath = documentsURL.appendingPathComponent("crp.key")

        // Make sure root cert exists
        //
        if !FileManager.default.fileExists(atPath: parentCertPath.path) || !FileManager.default.fileExists(atPath: parentKeyPath.path) {
            do {
                let rootCert = try createRootCertificate(organization: C2PAHelper.CERT_ORGANIZATION, validityDays: C2PAHelper.CERT_VALIDITY_DAYS)
                try rootCert.getCertificateBytes().write(to: parentCertPath, options: [.atomic])
                try rootCert.getPrivateKeyBytes().write(to: parentKeyPath, options: [.atomic])
                
                // Generated new root, so delete any other certs and regenerate (should not really happen)
                try? FileManager.default.removeItem(at: certPath)
                try? FileManager.default.removeItem(at: keyPath)
            } catch {
                return nil
            }
        }

        let rootCert = try? Certificate(certificateData: FileData(path: parentCertPath.path, bytes: Data(contentsOf: parentCertPath), fileName: parentCertPath.lastPathComponent), privateKeyData: FileData(path: parentKeyPath.path, bytes: Data(contentsOf: parentKeyPath), fileName: parentKeyPath.lastPathComponent), parentCertificate: nil)
        guard let rootCert = rootCert else { return nil }
        
        // Make sure user cert exists
        //
        if !FileManager.default.fileExists(atPath: certPath.path) || !FileManager.default.fileExists(atPath: keyPath.path) {
            do {
                let userCert = try createContentCredentialsCertificate(rootCertificate: rootCert, organization: C2PAHelper.CERT_ORGANIZATION_USER, validityDays: C2PAHelper.CERT_VALIDITY_DAYS)
                try userCert.getCertificateBytes().write(to: certPath, options: [.atomic])
                try userCert.getPrivateKeyBytes().write(to: keyPath, options: [.atomic])
            } catch {
                return nil
            }
        }

        let userCert = try? Certificate(certificateData: FileData(path: certPath.path, bytes: Data(contentsOf: certPath), fileName: certPath.lastPathComponent), privateKeyData: FileData(path: keyPath.path, bytes: Data(contentsOf: keyPath), fileName: keyPath.lastPathComponent), parentCertificate: rootCert)
        guard let userCert = userCert else { return nil }
        return (rootCert: rootCert, userCert: userCert)
    }
    
    /**
     Get the signing identity stored in settings, or default to the public key fingerprint.
     */
    private func getSigningIdentity() -> (email: String?, emailDisplay: String?, pgpFingerprint: String?, webLink: String?) {
        var identity = Settings.shared.credentialsPrimary
        if let i = identity {
            identity = i.replacingOccurrences(of: "@", with: " at ").addingPercentEncoding(withAllowedCharacters: .alphanumerics)! + "@mailto:" + i.addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        } else {
            let pubKey = Proof.shared.publicKeyFingerprint() ?? ""
            identity = "0x" + pubKey + "@https://keys.openpgp.org/search?q=" + pubKey
        }
        return (email: identity, emailDisplay: identity, pgpFingerprint: Proof.shared.publicKeyFingerprint(), nil)
    }
    
    public func signData(data: Data, filename: String, isCapture: Bool) -> (URL?, Data?) {
        let tempDirectoryURL = FileManager.default.temporaryDirectory
        let imagePath = tempDirectoryURL.appendingPathComponent(filename)
        let outputPath = imagePath.deletingPathExtension().deletingLastPathComponent().appendingPathComponent(imagePath.deletingPathExtension().lastPathComponent + "_c2pa", isDirectory: false).appendingPathExtension(imagePath.pathExtension)
        do {
            try? FileManager.default.removeItem(at: outputPath)
            try data.write(to: imagePath, options: [.atomic])
            if signFile(inputPath: imagePath, inputData: data, outputPath: outputPath, isCapture: isCapture) {
                let output = try? Data(contentsOf: outputPath, options: [.uncached])
                return (outputPath, output ?? data)
            }
        } catch {}
        return (nil, data)
    }

    public func signUrl(url: URL, filename: String, isCapture: Bool) -> URL {
        let tempDirectoryURL = FileManager.default.temporaryDirectory
        let inputPath = tempDirectoryURL.appendingPathComponent(filename)
        let outputPath = inputPath.deletingPathExtension().deletingLastPathComponent().appendingPathComponent(inputPath.deletingPathExtension().lastPathComponent + "_c2pa", isDirectory: false).appendingPathExtension(inputPath.pathExtension)
        do {
            try? FileManager.default.removeItem(at: outputPath)
            try FileManager.default.copyItem(at: url, to: inputPath)
            if signFile(inputPath: inputPath, inputData: nil, outputPath: outputPath, isCapture: isCapture) {
                return outputPath
            }
        } catch {}
        return url
    }
    
    private func signFile(inputPath: URL, inputData: Data?, outputPath: URL, isCapture: Bool) -> Bool {
        guard let (_, userCert) = getCertificates() else { return false }
        let signingIdentity = C2PAHelper.shared.getSigningIdentity()
        do {
            let fileData = FileData(path: inputPath.path, bytes: inputData, fileName: inputPath.lastPathComponent)
            
            let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? "ProofMode"
            let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
            let appInfo = ApplicationInfo(name: appName, version: appVersion, iconUri: C2PAHelper.APP_ICON_URI)
            
            let cc = ContentCredentials(certificate: userCert, file: fileData, applicationInfo: appInfo)
            if isCapture {
                try? cc.addCreatedAssertion()
            } else {
                try? cc.addPlacedAssertion()
            }
            if !Settings.shared.optionBlockAI {
                try? cc.addPermissiveAiTrainingAssertions()
            } else {
                try? cc.addRestrictedAiTrainingAssertions()
            }
            
            if let email = signingIdentity.email, let emailDisplay = signingIdentity.emailDisplay {
                try? cc.addEmailAssertion(email: email, displayName: emailDisplay)
            }
            if let pgpFingerprint = signingIdentity.pgpFingerprint {
                try? cc.addPgpAssertion(fingerprint: pgpFingerprint, displayName: pgpFingerprint)
            }
            if let webLink = signingIdentity.webLink {
                try? cc.addWebsiteAssertion(url: webLink)
            }
            
            // Location EXIF?
            if Settings.shared.optionLocation {
                let location = LocationManager.shared.getLocation()
                
                if let latS = location[.locationLatitude], let lonS = location[.locationLongitude], let lat = Double(latS), let lon = Double(lonS) {
                    let exifMake = appName
                    let exifModel = appVersion

                    let date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
                    let exifTimestamp = dateFormatter.string(from: date)
                    
                    let exifGpsVersion = "2.2.0.0"
                    var exifLat: String? = nil
                    var exifLong: String? = nil

                    let coords = coordinateToDMS(latitude: lat, longitude: lon)
                    exifLat = coords.latitude
                    exifLong = coords.longitude
                    
                    let exifData = ExifData(gpsVersionId: exifGpsVersion, latitude: exifLat, longitude: exifLong, altitudeRef: nil, altitude: nil, timestamp: exifTimestamp, speedRef: nil, speed: nil, directionRef: nil, direction: nil, destinationBearingRef: nil, destinationBearing: nil, positioningError: nil, exposureTime: nil, fNumber: nil, colorSpace: nil, digitalZoomRatio: nil, make: exifMake, model: exifModel, lensMake: nil, lensModel: nil, lensSpecification: nil)
                    try? cc.addExifAssertion(exifData: exifData)
                }
            }
            
            
            let _ = try cc.embedManifest(outputPath: outputPath.path)
            return true
        } catch {}
        return false
    }
    
    private func sha256(data: Data?) -> String? {
        guard let data = data else { return nil }
        return SHA256.hash(data: data).hexStr
    }
    
    // From: https://stackoverflow.com/questions/35120793/convert-mapkit-latitude-and-longitude-to-dms-format
    private func coordinateToDMS(latitude: Double, longitude: Double) -> (latitude: String, longitude: String) {
        let latDegrees = abs(Int(latitude))
        let latMinutes = abs(Int((latitude * 3600).truncatingRemainder(dividingBy: 3600) / 60))
        let latSeconds = Double(abs((latitude * 3600).truncatingRemainder(dividingBy: 3600).truncatingRemainder(dividingBy: 60)))

        let lonDegrees = abs(Int(longitude))
        let lonMinutes = abs(Int((longitude * 3600).truncatingRemainder(dividingBy: 3600) / 60))
        let lonSeconds = Double(abs((longitude * 3600).truncatingRemainder(dividingBy: 3600).truncatingRemainder(dividingBy: 60) ))

        return (String(format:"%d° %d' %.3f\" %@", latDegrees, latMinutes, latSeconds, latitude >= 0 ? "N" : "S"),
                String(format:"%d° %d' %.3f\" %@", lonDegrees, lonMinutes, lonSeconds, longitude >= 0 ? "E" : "W"))
    }
}

fileprivate extension Digest {
    var bytes: [UInt8] { Array(makeIterator()) }
    var data: Data { Data(bytes) }
    
    var hexStr: String {
        bytes.map { String(format: "%02X", $0) }.joined()
    }
}
