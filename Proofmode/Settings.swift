//
//  Settings.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-15.
//

import SwiftUI
import Photos
import DeviceCheck
import LibProofMode
import AppTrackingTransparency
import AuthenticationServices

class Settings: ObservableObject {
    static let shared = Settings()
    static let defaults = UserDefaults(suiteName: "group.org.witness.proofmode.ios") ?? UserDefaults.standard
        
    // MARK: -
    @Published var onboarded: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_ONBOARDED, defaultValue: false) {
        didSet { Settings.defaults.set(self.onboarded, forKey: UserDefaults.PREF_ONBOARDED) }
    }
    @Published var isOn: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_ON, defaultValue: false) {
        didSet { Settings.defaults.set(self.isOn, forKey: UserDefaults.PREF_OPTION_ON) }
    }
    @Published var optionNotary: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_NOTARY, defaultValue: UserDefaults.PREF_OPTION_NOTARY_DEFAULT) {
        didSet { Settings.defaults.set(self.optionNotary, forKey: UserDefaults.PREF_OPTION_NOTARY) }
    }
    @Published var optionLocation: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_LOCATION, defaultValue: UserDefaults.PREF_OPTION_LOCATION_DEFAULT) {
        didSet {
            Settings.defaults.set(self.optionLocation, forKey: UserDefaults.PREF_OPTION_LOCATION)
            if self.optionLocation {
                let cll = CLLocationManager()
                let authStatus = cll.authorizationStatus
                if authStatus == .notDetermined {
                    self.optionLocation = false
                    LocationManager.shared.getPermission { status in
                        if cll.authorizationStatus == .authorizedWhenInUse || status == .authorizedAlways {
                            self.optionLocation = true
                        }
                    }
                } else if cll.authorizationStatus != .authorizedAlways && cll.authorizationStatus != .authorizedWhenInUse {
                    // Reset, we don't have permission!
                    self.optionLocation = false
                }
            }
        }
    }
    @Published var optionPhone: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_PHONE, defaultValue: UserDefaults.PREF_OPTION_PHONE_DEFAULT) {
        didSet {
            Settings.defaults.set(self.optionPhone, forKey: UserDefaults.PREF_OPTION_PHONE)
            if self.optionPhone {
                let authStatus = ATTrackingManager.trackingAuthorizationStatus
                if authStatus == .notDetermined {
                    self.optionPhone = false
                    ATTrackingManager.requestTrackingAuthorization { status in
                        if status == .authorized {
                            self.optionPhone = true
                        }
                    }
                } else if authStatus != .authorized {
                    // Reset, we don't have permission!
                    self.optionPhone = false
                }
            }
        }
    }
    @Published var optionNetwork: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_NETWORK, defaultValue: UserDefaults.PREF_OPTION_NETWORK_DEFAULT) {
        didSet { Settings.defaults.set(self.optionNetwork, forKey: UserDefaults.PREF_OPTION_NETWORK) }
    }
    @Published var appIntegrityKey: String? = Settings.defaults.string(forKey: UserDefaults.PREF_APP_INTEGRITY_KEY) {
        didSet { Settings.defaults.set(self.appIntegrityKey, forKey: UserDefaults.PREF_APP_INTEGRITY_KEY) }
    }
    @Published var optionCredentials: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_CREDENTIALS, defaultValue: UserDefaults.PREF_OPTION_CREDENTIALS_DEFAULT) {
        didSet { Settings.defaults.set(self.optionCredentials, forKey: UserDefaults.PREF_OPTION_CREDENTIALS) }
    }
    @Published var optionBlockAI: Bool = Settings.defaults.boolWithDefault(UserDefaults.PREF_OPTION_AI, defaultValue: UserDefaults.PREF_OPTION_AI_DEFAULT) {
        didSet { Settings.defaults.set(self.optionBlockAI, forKey: UserDefaults.PREF_OPTION_AI) }
    }
    var credentialsPrimary: String? {
        get {
            var prefCredsPrimary: String? = nil
            let group = DispatchGroup()
            if let userAccountId = KeyChain.shared.get(account: "userId") {
                group.enter()
                let provider = ASAuthorizationAppleIDProvider()
                provider.getCredentialState(forUserID: userAccountId) { credentialState, error in
                    switch credentialState {
                    case .authorized:
                        prefCredsPrimary = KeyChain.shared.get(account: "prefCredsPrimary")
                    default: break
                    }
                    group.leave()
                }
            }
            group.wait()
            return prefCredsPrimary
        }
        set {
            let _ = KeyChain.shared.set(val: newValue, account: "prefCredsPrimary")
            if newValue == nil {
                // Removed. Remove the userId as well.
                let _ = KeyChain.shared.delete(account: "userId")
            }
        }
    }

    // Initialization
    private init() {
        // Need to migrate to shared settings?
        if !onboarded && UserDefaults.standard.boolWithDefault(UserDefaults.PREF_ONBOARDED, defaultValue: false) == true {
            onboarded = UserDefaults.standard.boolWithDefault(UserDefaults.PREF_ONBOARDED, defaultValue: false)
            isOn = UserDefaults.standard.boolWithDefault(UserDefaults.PREF_OPTION_ON, defaultValue: false)
            optionNotary = UserDefaults.standard.boolWithDefault(UserDefaults.PREF_OPTION_NOTARY, defaultValue: UserDefaults.PREF_OPTION_NOTARY_DEFAULT)
            optionLocation = UserDefaults.standard.boolWithDefault(UserDefaults.PREF_OPTION_LOCATION, defaultValue: UserDefaults.PREF_OPTION_LOCATION_DEFAULT)
            optionPhone = UserDefaults.standard.boolWithDefault(UserDefaults.PREF_OPTION_PHONE, defaultValue: UserDefaults.PREF_OPTION_PHONE_DEFAULT)
            optionNetwork = UserDefaults.standard.boolWithDefault(UserDefaults.PREF_OPTION_NETWORK, defaultValue: UserDefaults.PREF_OPTION_NETWORK_DEFAULT)
        }
        
        // Need an apple integrity key? Can be generated only if not running in extension.
        if !Bundle.main.bundlePath.hasSuffix(".appex") && appIntegrityKey == nil && DCAppAttestService.shared.isSupported {
            DCAppAttestService.shared.generateKey { keyId, error in
                guard error == nil else { return }
                DispatchQueue.main.async {
                    self.appIntegrityKey = keyId
                }
            }
        }
    }
}

struct optionNotaryKey: EnvironmentKey {
    static var defaultValue: Bool = false
}

struct assetKey: EnvironmentKey {
    static var defaultValue: PHAsset? = nil
}

extension EnvironmentValues {
    var optionNotary: Bool {
        get { self[optionNotaryKey.self] }
        set { self[optionNotaryKey.self] = newValue }
    }
    
    var asset: PHAsset? {
        get { self[assetKey.self] }
        set { self[assetKey.self] = newValue }
    }
}

extension UserDefaults {
    static let PREF_ONBOARDED = "onboarded"
    static let PREF_OPTION_ON = "isOn"
    static let PREF_OPTION_NOTARY = "autoNotarize"
    static let PREF_OPTION_LOCATION = "trackLocation"
    static let PREF_OPTION_PHONE = "trackDeviceId"
    static let PREF_OPTION_NETWORK = "trackMobileNetwork"
    static let PREF_APP_INTEGRITY_KEY = "appIntegrityKey"
    static let PREF_OPTION_CREDENTIALS = "addCR"
    static let PREF_OPTION_AI = "blockAI"
    
    static let PREF_OPTION_NOTARY_DEFAULT = true
    static let PREF_OPTION_LOCATION_DEFAULT = false
    static let PREF_OPTION_PHONE_DEFAULT = true
    static let PREF_OPTION_NETWORK_DEFAULT = true
    static let PREF_OPTION_CREDENTIALS_DEFAULT = true
    static let PREF_OPTION_AI_DEFAULT = true

    func boolWithDefault(_ key:String, defaultValue:Bool) -> Bool {
        if object(forKey: key) == nil {
            return defaultValue
        }
        return bool(forKey: key)
    }
    
    func reset() {
        removeObject(forKey: UserDefaults.PREF_ONBOARDED)
        removeObject(forKey: UserDefaults.PREF_OPTION_ON)
        removeObject(forKey: UserDefaults.PREF_OPTION_NOTARY)
        removeObject(forKey: UserDefaults.PREF_OPTION_LOCATION)
        removeObject(forKey: UserDefaults.PREF_OPTION_PHONE)
        removeObject(forKey: UserDefaults.PREF_OPTION_NETWORK)
        removeObject(forKey: UserDefaults.PREF_APP_INTEGRITY_KEY)
        removeObject(forKey: UserDefaults.PREF_OPTION_CREDENTIALS)
        removeObject(forKey: UserDefaults.PREF_OPTION_AI)
    }
}
