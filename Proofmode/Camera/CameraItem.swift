//
//  CameraItem.swift
//  Proofmode
//
//  Created by N-Pex on 2023-01-24.
//

import SwiftUI
import Photos
import LibProofMode
import LegacyUTType
import CryptoKit

enum CameraItemState {
    case initial
    case saving
    case savedToLibrary(String)
    case failed(String)
    case deleted(String)
}

class CameraItem: MediaItem, ObservableObject, Identifiable, Codable {
    var state: CameraItemState = .initial {
        didSet {
            DispatchQueue.main.async {
                self.objectWillChange.send()
            }
        }
    }
    
    override var isGeneratingProof: Bool {
        didSet {
            DispatchQueue.main.async {
                self.objectWillChange.send()
            }
        }
    }
    
    func generateProof() {
        Proof.shared.process(mediaItem: self, options: Settings.shared.toProofGenerationOptions()) { _ in
            // Send change event, so UI can stop showing spinner etc.
            self.objectWillChange.send()
        }
    }
    
    var id: String = ""
    var localAssetIdentifier: String? = nil
    
    init(mediaItemHash: String?, assetIdentifier: String) {
        localAssetIdentifier = assetIdentifier
        let results = PHAsset.fetchAssets(withLocalIdentifiers: [assetIdentifier], options: nil)
        if let first = results.firstObject {
            super.init(asset: first)
        } else {
            state = .deleted(assetIdentifier)
            super.init(mediaData: Data())
        }
        self.mediaItemHash = mediaItemHash
        setProofFolder()
    }
    
    override init(asset: PHAsset) {
        self.localAssetIdentifier = asset.localIdentifier
        super.init(asset: asset)
        setProofFolder()
    }
    
    init(_ mediaItemHash: String?, mediaUrl: URL, mediaType: (any UTTypeProtocol)? = nil, save: Bool = false, fileName: String? = nil, whenSaved: ((CameraItem) -> Void)? = nil) {
        super.init(mediaUrl: mediaUrl, mediaType: mediaType)
        self.mediaItemHash = mediaItemHash
        setProofFolder()
        if save {
            DispatchQueue.global(qos: .userInitiated).async {
                self.save(whenSaved)
            }
        }
    }
    
    init(_ mediaItemHash: String?, mediaData: Data, mediaType: (any UTTypeProtocol)? = nil, save: Bool = false, modifiedDate: Date? = nil, fileName: String? = nil, whenSaved: ((CameraItem) -> Void)? = nil) {
        super.init(mediaData: mediaData, mediaType: mediaType, modifiedDate: modifiedDate)
        self.mediaItemHash = mediaItemHash
        setProofFolder()
        if save {
            DispatchQueue.global(qos: .userInitiated).async {
                self.save(whenSaved)
            }
        }
    }

    func calculateHash(callback:@escaping (CameraItem,String?) -> Void) {
        self.withData { data in
            self.data = data
            let mediaHash = Proof.shared.sha256(data: data)
            self.mediaItemHash = mediaHash
            callback(self, mediaHash)
        }
    }
    
    private func setProofFolder() {
        let documentsDirectory = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.witness.proofmode.ios")
             ?? FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        if let mediaItemHash = self.mediaItemHash {
            self.proofFolder = documentsDirectory.appendingPathComponent(mediaItemHash, isDirectory: true)
        }
        //self.proofFilesBaseName = self.mediaItemHash
    }
    
    // Codable protocol conformance
    //
    enum CodingKeys: String, CodingKey {
        case assetIdentifier, mediaHash, mediaUrl, mediaType
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(mediaItemHash, forKey: CodingKeys.mediaHash)
        try container.encodeIfPresent(localAssetIdentifier, forKey: CodingKeys.assetIdentifier)
        try container.encodeIfPresent(mediaUrl, forKey: CodingKeys.mediaUrl)
        if let mediaType = mediaType {
            try mediaType.encode(to: container.superEncoder(forKey: CodingKeys.mediaType))
        }
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let mediaItemHash = try container.decode(String.self, forKey: CodingKeys.mediaHash)
        if let assetIdentifier = try? container.decodeIfPresent(String.self, forKey: CodingKeys.assetIdentifier) {
            self.init(mediaItemHash: mediaItemHash, assetIdentifier: assetIdentifier)
            return
        } else if
            let url = try? container.decodeIfPresent(URL.self, forKey: CodingKeys.mediaUrl),
            let decoder = try? container.superDecoder(forKey: CodingKeys.mediaType),
            let type = try? UTType(from: decoder) {
            self.init(mediaItemHash, mediaUrl: url, mediaType: type)
            return
        }
        throw ActivityError.failedToDecodeActivity
    }
    
    
    private func setState(_ state: CameraItemState) {
        if Thread.current.isMainThread {
            self.state = state
        } else {
            DispatchQueue.main.async {
                self.state = state
            }
        }
    }
    
    public var creationDate: Date? {
        if let asset = self.asset {
            return asset.creationDate
        }
        return self.modifiedDate
    }
    
    /**
     If we have write access to the photo library, save there.
     */
    func save(_ whenSaved: ((CameraItem) -> Void)? = nil) {
        let acceptedAuthLevel = [PHAuthorizationStatus.authorized, PHAuthorizationStatus.limited]
        self.setState(.saving)
        if acceptedAuthLevel.contains(PHPhotoLibrary.authorizationStatus(for: .readWrite)) {
            self.saveToLibrary(whenSaved)
        } else if PHPhotoLibrary.authorizationStatus(for: .readWrite) == .notDetermined {
            // Ask the user
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
                if acceptedAuthLevel.contains(status) {
                    self.saveToLibrary(whenSaved)
                } else {
                    self.setState(.failed("No photo library access"))
                    whenSaved?(self)
                }
            }
        } else {
            self.setState(.failed("No photo library access"))
            whenSaved?(self)
        }
    }
    
    private func saveToLibrary(_ whenSaved: ((CameraItem) -> Void)?) {
        if let data = self.data, self.mediaType?.utType == .image {
            storeImage(data: data) { assetIdentifier in
                if let assetIdentifier = assetIdentifier, let asset = PHAsset.fetchAssets(withLocalIdentifiers: [assetIdentifier], options: nil).firstObject {
                    self.asset = asset
                    self.calculateHash { _, _ in
                        self.setProofFolder()
                        whenSaved?(self)
                        self.setState(.savedToLibrary(assetIdentifier))
                    }
                } else {
                    self.setState(.failed("Failed to save"))
                    whenSaved?(self)
                }
            }
        } else if let url = self.mediaUrl, self.mediaType?.utType == .image {
            storeImageFromFile(url: url) { assetIdentifier in
                if let assetIdentifier = assetIdentifier, let asset = PHAsset.fetchAssets(withLocalIdentifiers: [assetIdentifier], options: nil).firstObject {
                    self.asset = asset
                    self.calculateHash { _, _ in
                        self.setProofFolder()
                        whenSaved?(self)
                        self.setState(.savedToLibrary(assetIdentifier))
                    }
                } else {
                    self.setState(.failed("Failed to save"))
                    whenSaved?(self)
                }
            }

        } else if let url = self.mediaUrl {
            storeVideo(video: url) { assetIdentifier in
                if let assetIdentifier = assetIdentifier, let asset = PHAsset.fetchAssets(withLocalIdentifiers: [assetIdentifier], options: nil).firstObject {
                    self.asset = asset
                    self.calculateHash { _, _ in
                        self.setProofFolder()
                        whenSaved?(self)
                        self.setState(.savedToLibrary(assetIdentifier))
                    }
                } else {
                    self.setState(.failed("Failed to save"))
                    whenSaved?(self)
                }
            }
        } else {
            self.setState(.failed("No input data"))
            whenSaved?(self)
        }
    }
    
    private func storeImage(data:Data, completion:@escaping (String?) -> Void){
        var createdObject: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetCreationRequest.forAsset()
            let options = PHAssetResourceCreationOptions()
            options.uniformTypeIdentifier = self.mediaType?.utType?.identifier
            options.shouldMoveFile = true
            request.addResource(with: .photo, data: data, options: options)
            createdObject = request.placeholderForCreatedAsset
            self.localAssetIdentifier = createdObject?.localIdentifier
        }) { saved, error in
            completion(saved ? createdObject?.localIdentifier : nil)
        }
    }

    private func storeImageFromFile(url:URL, completion:@escaping (String?) -> Void){
        var createdObject: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetCreationRequest.forAsset()
            let options = PHAssetResourceCreationOptions()
            options.uniformTypeIdentifier = self.mediaType?.utType?.identifier
            options.shouldMoveFile = true
            request.addResource(with: .photo, fileURL: url, options: options)
            createdObject = request.placeholderForCreatedAsset
            self.localAssetIdentifier = createdObject?.localIdentifier
        }) { saved, error in
            completion(saved ? createdObject?.localIdentifier : nil)
        }
    }
    
    private func storeVideo(video:URL,completion:@escaping (String?) -> Void){
        var createdObject: PHObjectPlaceholder?
        
        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: video)
            createdObject = request?.placeholderForCreatedAsset
            self.localAssetIdentifier = createdObject?.localIdentifier
        }) { saved, error in
            completion(saved ? createdObject?.localIdentifier : nil)
        }
    }
}

class CameraItems: ObservableObject {
    @Published var cameraItems: [CameraItem]
    
    init(cameraItems:[CameraItem]) {
        self.cameraItems = cameraItems
    }
    
    func add(_ cameraItem: CameraItem) {
        self.cameraItems.append(cameraItem)
    }
}

class ObservedCameraItems: ObservableObject {
    var cameraItems: [CameraItem]
    private var cancellables = [AnyObject]()
    
    init() {
        cameraItems = []
    }
    
    func setItems(cameraItems:[CameraItem]) {
        self.cameraItems = cameraItems
        self.cameraItems.forEach({
            let c = $0.objectWillChange.sink(receiveValue: {
                self.objectWillChange.send()
            })
            
            // Important: You have to keep the returned value allocated,
            // otherwise the sink subscription gets cancelled
            self.cancellables.append(c)
        })
        self.objectWillChange.send()
    }
}
