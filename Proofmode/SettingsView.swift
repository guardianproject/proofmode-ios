//
//  SettingsView.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-17.
//

import SwiftUI
import AppTrackingTransparency
import CoreLocation
import AuthenticationServices

struct SettingsView: View {
    @EnvironmentObject var settings: Settings
    
    @State private var showOptionLocationDeniedAlert: Bool = false
    @State private var showOptionPhoneDeniedAlert: Bool = false
    
    @State private var optionLocation: Bool = UserDefaults.PREF_OPTION_LOCATION_DEFAULT
    @State private var optionPhone: Bool = UserDefaults.PREF_OPTION_PHONE_DEFAULT
    @State private var optionCredentials: Bool = UserDefaults.PREF_OPTION_CREDENTIALS_DEFAULT

    @State private var accountPickerDelegate: AccountPickerDelegate? = nil
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading, spacing: 0) {
                    Text("Strengthen your media’s verifiability by adding data to your proof.")
                        .font(.custom(Font.titleFont, size: 21))
                        .fontWeight(.bold)
                        .foregroundColor(Color("colorDarkGray"))
                        .multilineTextAlignment(.center)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .padding()
                    
                    Text("Tap the icons to add or remove.")
                        .font(.custom(Font.titleFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.black)
                        .multilineTextAlignment(.center)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .padding()
                    
                    HStack {
                        SettingsTileView(imageName: "ic_notary", title: "Notary", content: "Registration with a third party that your media exists", isSelected: self.$settings.optionNotary)
                            .frame(width: geometry.size.width / 2, height: 200)
                        SettingsTileView(imageName: "ic_location-pin", title: "Location", content: "GPS coordinates when media is taken", isSelected: self.$optionLocation)
                            .frame(width: geometry.size.width / 2, height: 200)
                    }
                    
                    HStack {
                        SettingsTileView(imageName: "ic_phone", title: "Phone", content: "Identification number of your phone", isSelected: self.$optionPhone)
                            .frame(width: geometry.size.width / 2, height: 200)
                        SettingsTileView(imageName: "ic_network", title: "Network", content: "Name of the network(s) you’re connected to", isSelected: self.$settings.optionNetwork)
                            .frame(width: geometry.size.width / 2, height: 200)
                    }
                    
                    HStack {
                        SettingsTileView(imageName: "ic_user", title: "Credentials", content: "Add Content Credentials (C2PA) to photos and videos", isSelected: self.$optionCredentials)
                            .frame(width: geometry.size.width / 2, height: 200)
                        SettingsTileView(imageName: "ic_flag", title: "Block AI", content: "Flag to not be used for machine learning training", isSelected: self.$settings.optionBlockAI)
                            .allowsHitTesting(self.settings.optionCredentials) // Not .disabled, that'll make the icon gray
                            .frame(width: geometry.size.width / 2, height: 200)
                    }

                }
                .frame(width: geometry.size.width, alignment: .top)
            }
            .content
            .frame(height: geometry.size.height, alignment: .top)
        }
        .navigationBarTitle("Choose your settings", displayMode: .inline)
        .onAppear {
            self.optionLocation = settings.optionLocation
            self.optionPhone = settings.optionPhone
            self.optionCredentials = settings.optionCredentials
        }
        .onChange(of: optionLocation) { newValue in
            if newValue {
                let cll = CLLocationManager()
                let authStatus = cll.authorizationStatus
                if authStatus == .denied {
                    showOptionLocationDeniedAlert = true
                    self.optionLocation = false
                    return
                }
            }
            if newValue != settings.optionLocation {
                settings.optionLocation = newValue
            }
        }
        .onChange(of: optionPhone) { newValue in
            if newValue {
                let authStatus = ATTrackingManager.trackingAuthorizationStatus
                if authStatus == .denied {
                    showOptionPhoneDeniedAlert = true
                    self.optionPhone = false
                    return
                }
            }
            if newValue != settings.optionPhone {
                settings.optionPhone = newValue
            }
        }
        .onChange(of: optionCredentials) { newValue in
            settings.optionCredentials = newValue
            if newValue {
                selectAccountForCR()
            } else {
                forgetAccountForCR()
                settings.optionBlockAI = UserDefaults.PREF_OPTION_AI_DEFAULT
            }
        }
        .alert(
            "Location access needed",
            isPresented: self.$showOptionLocationDeniedAlert
        ) {
            Button("Cancel") {
                self.optionLocation = false
            }
            Button("Open settings") {
                // Open privacy settings
                guard let url = URL(string: UIApplication.openSettingsURLString),
                      UIApplication.shared.canOpenURL(url) else {
                    assertionFailure("Not able to open App privacy settings")
                    return
                }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } message: {
            Text("To enable this feature you need to allow location access to the app in system settings.")
        }
        .alert(
            "Phone identifier access needed",
            isPresented: self.$showOptionPhoneDeniedAlert
        ) {
            Button("Cancel") {
                self.optionPhone = false
            }
            Button("Open settings") {
                // Open privacy settings
                guard let url = URL(string: UIApplication.openSettingsURLString),
                      UIApplication.shared.canOpenURL(url) else {
                    assertionFailure("Not able to open App privacy settings")
                    return
                }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } message: {
            Text("To enable this feature you need to allow app tracking access to the app in system settings.")
        }
    }
    
    private func selectAccountForCR() {
        let provider = ASAuthorizationAppleIDProvider()
        if Settings.shared.credentialsPrimary != nil {
            // Already set!
            return
        }
        if accountPickerDelegate == nil {
            accountPickerDelegate = AccountPickerDelegate(parent: self)
        }
        let request = provider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = accountPickerDelegate
        controller.performRequests()
    }
    
    private func forgetAccountForCR() {
        Settings.shared.credentialsPrimary = nil
    }
}

class AccountPickerDelegate: NSObject, ASAuthorizationControllerDelegate {
    var parent: SettingsView?
    init(parent: SettingsView) {
        self.parent = parent
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
            let name = appleIDCredential.fullName
            let email = appleIDCredential.email
            var namePart = ""
            if let name = name {
                namePart = name.formatted()
            }
            let emailPart = email != nil ? (namePart.count > 0 ? " (\(email!)" : email!) : ""
            let _ = KeyChain.shared.set(val: userIdentifier, account: "userId")
            Settings.shared.credentialsPrimary = namePart + emailPart

        default:
            break
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        Settings.shared.credentialsPrimary = nil
    }
}

struct SettingsTileView: View {
    var imageName: String
    var title: String
    var content: String
    @Binding var isSelected: Bool
    
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Button(action: {
                self.isSelected.toggle()
            }) {
                ZStack {
                    Circle().stroke(isSelected ? Color("colorPrimary") : Color.gray, lineWidth: isSelected ? 2 : 0.65)
                        .frame(width: 39, height: 39)
                    Image(imageName)
                        .resizable()
                        .frame(width: 20, height: 20, alignment: .center)
                        .accentColor(isSelected ? Color("colorPrimary") : Color.gray)
                    if isSelected {
                        Image("ic_selected")
                            .resizable()
                            .frame(width: 11, height: 11, alignment: .center)
                            .offset(x: 17, y: 14)
                            .accentColor(Color("colorPrimary"))
                    }
                }
            }
            Text(title)
                .font(Font.system(size: 13).smallCaps())
                .fontWeight(.regular)
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .lineLimit(nil)
            
            Text(content)
                .font(Font.system(size: 13))
                .fontWeight(.regular)
                .foregroundColor(.gray)
                .multilineTextAlignment(.center)
                .lineLimit(nil)
        }.padding()
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
