//
//  ProofZipHelper.swift
//  Proofmode
//
//  Created by N-Pex on 2023-08-18.
//

// TODO - Move this to LibProofMode?

import LibProofMode
import ZIPFoundation

class ProofZipHelper {
    static func createProofZip(fileName: String?, assets: [CameraItem], addMedia: Bool) -> (URL, [String:String])? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        let directoryUrl = urls[0]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ssz"
        let fingerprint = Proof.shared.publicKeyFingerprint()
        let prefix = fileName ?? "proofmode"
        let fileName = "\(prefix)-\(fingerprint ?? "")-\(dateFormatter.string(from: Date())).zip"
        let destinationURL = directoryUrl.appendingPathComponent(fileName)
        if fileManager.fileExists(atPath: destinationURL.path) {
            // Zip exists
            print("Zip already exists, use that so we get same hash!")
            return (destinationURL, [:])
        } else {
            do {
                let archive = Archive(url: destinationURL, accessMode: .create)
                
                var batchProof:[String] = []
                var mediaFilenameToHashMap:[String:String] = [:]
                
                for mediaItem in assets {
                    if let proofFolder = Proof.shared.proofFolder(for: mediaItem) {
                        let files = try fileManager.contentsOfDirectory(at: proofFolder, includingPropertiesForKeys: [])
                        for file in files {
                            try archive?.addEntry(with: file.lastPathComponent, fileURL: file)
                            if assets.count > 1, file.lastPathComponent.hasSuffix(".csv") {
                                do {
                                    // Add CSV data line to batch proof
                                    let data = try String(contentsOf: file, encoding: .utf8)
                                    let lines = data.components(separatedBy: .newlines)
                                    if batchProof.isEmpty, lines.count > 0 {
                                        // Get header line from proof
                                        batchProof.append(lines[0])
                                    }
                                    if (lines.count > 1) {
                                        batchProof.append(lines[1])
                                    }
                                } catch {
                                    print(error)
                                }
                            }
                        }
                    }
                    
                    // Send original image as well?
                    if addMedia, let data = mediaItem.data, data.count > 0 {
                        mediaFilenameToHashMap[mediaItem.filenameOrDefault] = mediaItem.mediaItemHash
                        try archive?.addEntry(with: mediaItem.filenameOrDefault, type: .file, uncompressedSize: Int64(data.count), modificationDate: mediaItem.modifiedDate ?? Date(), permissions: nil, compressionMethod: .none, bufferSize: data.count, progress: nil, provider: { (position: Int64, size: Int) in
                            return data.subdata(in: Data.Index(position)..<(Int(position) + size))
                        })
                    }
                }
                
                // Have we generated batch proof?
                if batchProof.count > 0 {
                    let fileName = "\(1000 * Date().timeIntervalSince1970)batchproof.csv"
                    if let data = batchProof.joined(separator: "\n").data(using: .utf8) {
                        try archive?.addEntry(with: fileName, type: .file, uncompressedSize: Int64(data.count), modificationDate: Date(), permissions: nil, compressionMethod: .none, bufferSize: data.count, progress: nil, provider: { (position: Int64, size: Int) in
                            return data.subdata(in: Data.Index(position)..<(Int(position) + size))
                        })
                    }
                }
                
                // Send public key
                if let key = Proof.shared.getPublicKeyString() {
                    let data = Data(key.utf8)
                    try archive?.addEntry(with: "pubkey.asc", type: .file, uncompressedSize: Int64(data.count), modificationDate: Date(), permissions: nil, compressionMethod: .none, bufferSize: data.count, progress: nil, provider: { (position: Int64, size: Int) in
                        return data.subdata(in: Data.Index(position)..<(Int(position) + size))
                    })
                }
                
                if let res = Bundle.main.url(forResource: "HowToVerifyProofData", withExtension: "txt") {
                    let data = try Data(contentsOf: res)
                    try archive?.addEntry(with: "HowToVerifyProofData.txt", type: .file, uncompressedSize: Int64(data.count), modificationDate: Date(), permissions: nil, compressionMethod: .none, bufferSize: data.count, progress: nil, provider: { (position: Int64, size: Int) in
                        return data.subdata(in: Data.Index(position)..<(Int(position) + size))
                    })
                }
                return (destinationURL, mediaFilenameToHashMap)
            } catch {
                print("Failed to create zip:\(error)")
            }
        }
        return nil
    }
}

