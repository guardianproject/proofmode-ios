//
//  KeyChain.swift
//  Proofmode
//
//  Created by N-Pex on 2023-11-10.
//

import Foundation

class KeyChain {
    static let service = "org.witness.proofmode.ios"
    static let shared = KeyChain()
    
    private init() {
    }
    
    func set(val: String?, account: String) -> Bool {
        if let val = val, let data = val.data(using: .utf8) {
            let query = [
                kSecValueData: data,
                kSecClass: kSecClassGenericPassword,
                kSecAttrService: KeyChain.service,
                kSecAttrAccount: account,
            ] as CFDictionary
            
            var status = SecItemAdd(query, nil)
            if status == errSecDuplicateItem {
                let query = [
                    kSecAttrService: KeyChain.service,
                    kSecAttrAccount: account,
                    kSecClass: kSecClassGenericPassword,
                ] as CFDictionary
                // Update existing item
                status = SecItemUpdate(query, [kSecValueData: data] as CFDictionary)
            }
            return status == errSecSuccess
        } else {
            return delete(account: account)
        }
    }
    
    func get(account: String) -> String? {
        let query = [
            kSecAttrService: KeyChain.service,
            kSecAttrAccount: account,
            kSecClass: kSecClassGenericPassword,
            kSecReturnData: true
        ] as CFDictionary
        
        var itemCopy: AnyObject?
        if SecItemCopyMatching(query, &itemCopy) != errSecSuccess {
            return nil
        }
        if let stringData = itemCopy as? Data, let s = String(data: stringData, encoding: .utf8) {
            return s
        }
        return nil
    }
    
    func delete(account: String) -> Bool {
        let query = [
            kSecAttrService: KeyChain.service,
            kSecAttrAccount: account,
            kSecClass: kSecClassGenericPassword,
        ] as CFDictionary
        return SecItemDelete(query) == errSecSuccess
    }
}
