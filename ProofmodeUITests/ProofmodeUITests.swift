//
//  ProofmodeUITests.swift
//  ProofmodeUITests
//
//  Created by N-Pex on 2023-02-07.
//

import XCTest

final class ProofmodeUITests: XCTestCase {
    let app = XCUIApplication()
    let timeout = TimeInterval(5)
            
    override func setUpWithError() throws {
        continueAfterFailure = false
        app.terminate()
    }

    override func tearDownWithError() throws {
    }

    func test001_Onboarding() throws {
        app.resetAuthorizationStatus(for: .userTracking)
        app.resetAuthorizationStatus(for: .camera)
        app.resetAuthorizationStatus(for: .photos)
        XCUIApplication().launchArguments += ["-AppleLanguages", "(en)"]
        XCUIApplication().launchArguments += ["-AppleLocale", "en_US"]
        app.launchArguments = ["UI_TEST_MODE_RESET_SETTINGS"]
        app.launch()
        
        addUIInterruptionMonitor(withDescription: "Photos Access Alert") { (alert) -> Bool in
            if alert.label == "“Proofmode” Would Like to Access Your Photos" {
                alert.buttons["Allow Access to All Photos"].tap()
                return true
            }
            if alert.label == "“Proofmode” Would Like to Access the Camera" {
                alert.buttons["Allow"].tap()
                return true
            }
            return false
        }
        
        let getStartedButton = app.buttons["Get Started"]
        if getStartedButton.waitForExistence(timeout: timeout) {
            getStartedButton.tap()
        } else {
            XCTFail("Did not find 'get started' button")
        }
        
        let nextButton = app.buttons["next"]
        let _ = nextButton.waitForExistence(timeout: timeout)
        XCTAssertTrue(nextButton.exists)
        nextButton.tap()
        nextButton.tap()
        nextButton.tap()
        nextButton.tap()
        
        let continueButton = app.buttons["Continue"]
        let _ = continueButton.waitForExistence(timeout: timeout)
        XCTAssertTrue(continueButton.exists)
        continueButton.tap()
        
        let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")
        let allowButton = springboard.buttons["Allow"]
        let _ = allowButton.waitForExistence(timeout: timeout)
        XCTAssertTrue(allowButton.exists)
        allowButton.tap()
        
        let title = app.staticTexts["Proof Log"]
        let _ = title.waitForExistence(timeout: timeout)
        XCTAssertTrue(title.exists, "Expected main app status!")
        
        sleep(2)
        let _ = allowButton.waitForExistence(timeout: timeout)
        XCTAssertTrue(allowButton.exists)
        allowButton.tap()
    }
    
    func test002_CameraWithNoPhotoLibPermission() throws {
        app.resetAuthorizationStatus(for: .camera)
        app.resetAuthorizationStatus(for: .photos)
        XCUIApplication().launchArguments += ["-AppleLanguages", "(en)"]
        XCUIApplication().launchArguments += ["-AppleLocale", "en_US"]
        app.launch()

        let title = app.staticTexts["Proof Log"]
        let _ = title.waitForExistence(timeout: timeout)
        XCTAssertTrue(title.exists, "Expected main app status!")

        // Wait for photo lib alert and press 'Don´t Allow´
        // Apple bug: Can't use "label == 'Don´t Allow'" in the predicate below, they probably
        // mess up because of the apostrophe used...
        let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")
        let predicate = NSPredicate(format: "label CONTAINS 'Don'")
        let doontAllowButton = springboard.buttons.matching(predicate).firstMatch
        if doontAllowButton.waitForExistence(timeout: timeout) {
            doontAllowButton.tap()
        } else {
            XCTFail("Did not find the 'Don´t Allow' option in the photo library access dialog")
        }
    }
    
    func test003_OpenSettings() throws {
        XCUIApplication().launchArguments += ["-AppleLanguages", "(en)"]
        XCUIApplication().launchArguments += ["-AppleLocale", "en_US"]
        app.launch()

        let title = app.staticTexts["Proof Log"]
        let _ = title.waitForExistence(timeout: timeout)
        XCTAssertTrue(title.exists, "Expected main app status!")
        
        let menuButton = app.buttons["menuButton"]
        if menuButton.waitForExistence(timeout: timeout) {
            menuButton.tap()
        } else {
            XCTFail("Failed to find menu button")
        }

        let settingsButton = app.buttons["more_settingsButton"]
        if settingsButton.waitForExistence(timeout: timeout) {
            settingsButton.tap()
        } else {
            XCTFail("Failed to find settings button")
        }

        let settingsTitle = app.staticTexts["Choose your settings"]
        let _ = settingsTitle.waitForExistence(timeout: timeout)
        XCTAssertTrue(settingsTitle.exists, "Expected settings screen!")
    }
}

// From https://www.jessesquires.com/blog/2021/10/25/delete-app-during-ui-tests/
extension XCUIApplication {
    func uninstall(name: String? = nil) {
        self.terminate()

        let timeout = TimeInterval(5)
        let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")

        let appName: String
        if let name = name {
            appName = name
        } else {
            let uiTestRunnerName = Bundle.main.infoDictionary?["CFBundleName"] as! String
            appName = uiTestRunnerName.replacingOccurrences(of: "UITests-Runner", with: "")
        }

        /// use `firstMatch` because icon may appear in iPad dock
        let appIcon = springboard.icons[appName].firstMatch
        if appIcon.waitForExistence(timeout: timeout) {
            appIcon.press(forDuration: 2)
        } else {
            return //Not installed
            //XCTFail("Failed to find app icon named \(appName)")
        }

        let removeAppButton = springboard.buttons["Remove App"]
        if removeAppButton.waitForExistence(timeout: timeout) {
            removeAppButton.tap()
        } else {
            XCTFail("Failed to find 'Remove App'")
        }

        let deleteAppButton = springboard.alerts.buttons["Delete App"]
        if deleteAppButton.waitForExistence(timeout: timeout) {
            deleteAppButton.tap()
        } else {
            XCTFail("Failed to find 'Delete App'")
        }

        let finalDeleteButton = springboard.alerts.buttons["Delete"]
        if finalDeleteButton.waitForExistence(timeout: timeout) {
            finalDeleteButton.tap()
        } else {
            XCTFail("Failed to find 'Delete'")
        }
    }
}
