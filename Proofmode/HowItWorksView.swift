//
//  HowItWorksView.swift
//  Proofmode
//
//  Created by N-Pex on 2020-07-15.
//

import SwiftUI
import CoreLocation
import LibProofMode
import AppTrackingTransparency

struct Page {
    var step: String
    var title: String
    var imageName: String
    var content: String
}

struct HowItWorksView: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    @State var onboarding: Bool
    @State var currentIndex = 0
    @State var dragOffset: CGFloat = 0.0
    var showBackButton: Bool {
        return onboarding && currentIndex > 0
    }
    
    var pages = [
        Page(step: "Step 1", title: "Getting proof is easy. Once ProofMode is on, use your camera as usual.", imageName: "ill_tut01", content: "ProofMode works in the background to generate and save proof. You don\'t have to do anything special!"),
        Page(step: "Step 2", title: "Once you’ve taken a photo or video, find it in your camera gallery to share it.", imageName: "ill_tut02", content: "Select a photo, tap the share icon and choose ‘Share Proof’."),
        Page(step: "Step 3", title: "Before you send, choose what level of proof to share.", imageName: "ill_tut03", content: "You\'re always in control. Basic shares minimal info to prove your media exists. Robust shares everything captured."),
        Page(step: "Step 4", title: "Then choose how to send it.", imageName: "ill_tut04", content: "You can share lots of ways, from email to messaging services like WhatsApp!"),
    ]
    
    var title:String {
        var idx = self.currentIndex
        if self.onboarding {
            idx -= 1
        }
        if idx < 0 || idx >= self.pages.count {
            return ""
        }
        return self.pages[idx].step
    }
    
    var numPages:Int {
        return self.pages.count + (self.onboarding ? 2 : 0)
    }
    
    var currentDot: Binding<Int> { Binding (
        get: { self.currentIndex - (self.onboarding ? 1 : 0) },
        set: { self.currentIndex = $0 + (self.onboarding ? 1 : 0) }
    )
    }
    
    var showIndicator:Bool {
        return self.currentDot.wrappedValue >= 0 && self.currentDot.wrappedValue < self.pages.count
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .bottomLeading) {
                // Pager
                HStack(alignment: .top, spacing: 0) {
                    // Welcome view
                    if self.onboarding {
                        WelcomeView(currentIndex: self.$currentIndex)
                            .frame(width: geometry.size.width, height: geometry.size.height)
                        
                    }
                    
                    ForEach(self.pages, id: \.step) { page in
                        PageView(page: page, currentIndex: self.$currentIndex)
                            .padding(.top, geometry.safeAreaInsets.top)
                            .padding(.bottom, 60 + geometry.safeAreaInsets.bottom)
                            .frame(width: geometry.size.width, height: geometry.size.height)
                            .clipped()
                    }
                    
                    // Setup view
                    if self.onboarding {
                        self.setupView
                            .padding(0)
                            .frame(width: geometry.size.width, height: geometry.size.height)
                            .clipped()
                    }
                    
                }
                .frame(height: geometry.size.height)
                .offset(x: self.dragOffset + CGFloat(self.currentIndex) * -geometry.size.width, y: 0)
                
                // Indicator
                if self.showIndicator {
                    ZStack {
                        HStack(spacing: 8) {
                            ForEach(0..<self.pages.count) { index in
                                PageIndicator(isSelected: Binding<Bool>(get: { self.currentDot.wrappedValue == index }, set: { _ in })) {
                                    withAnimation {
                                        self.currentDot.wrappedValue = index
                                    }
                                }
                            }
                        }
                        
                        HStack {
                            Spacer()
                            Button(action: {
                                if self.currentIndex < (self.numPages - 1) {
                                    withAnimation {
                                        self.currentIndex += 1
                                    }
                                } else {
                                    // Done
                                    self.mode.wrappedValue.dismiss()
                                }
                            }) {
                                Image(systemName: "chevron.right")
                                    .resizable()
                                    .scaledToFit()
                                    .accentColor(.black)
                                    .frame(width: 24, height: 24)
                                    .padding(.trailing, 10)
                            }.accessibilityIdentifier("next")
                        }
                    }
                    .frame(width: geometry.size.width, height: 60)
                    .padding(.bottom, 10)
                }
            }
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .topLeading)
            .background(Color("colorOnboardingBackground").ignoresSafeArea())
            .gesture(
                DragGesture()
                    .onChanged {
                        if self.currentIndex > 0 && $0.translation.width > 0 {
                            self.dragOffset = $0.translation.width
                        } else if self.currentIndex < (self.numPages - 1) && $0.translation.width < 0 {
                            self.dragOffset = $0.translation.width
                        }
                    }
                    .onEnded{ value in
                        self.handleDrag()
                    }
                , including: .all)
            .navigationBarTitle(LocalizedStringKey(self.title), displayMode: .inline)
            .navigationBarBackButtonHidden(self.onboarding)
            .navigationBarItems(leading: self.showBackButton ?
                                AnyView(self.backButton) : AnyView(EmptyView()))
            //.edgesIgnoringSafeArea(.all)
        }
        //.edgesIgnoringSafeArea(.all)
        //.background(Color("colorOnboardingBackground"))
    }
    
    func handleDrag() {
        if self.onboarding, self.currentIndex > 1, self.dragOffset > 30 {
            withAnimation {
                self.dragOffset = 0
                self.currentIndex -= 1
            }
        } else if self.onboarding, self.currentIndex > 1, self.currentIndex < (self.numPages - 1), self.dragOffset < -30 {
            withAnimation {
                self.dragOffset = 0
                self.currentIndex += 1
            }
        } else if !self.onboarding, self.currentIndex > 0 && self.dragOffset > 30 {
            withAnimation {
                self.dragOffset = 0
                self.currentIndex -= 1
            }
        } else if !self.onboarding, self.currentIndex < (self.pages.count - 1) && self.dragOffset < -30 {
            withAnimation {
                self.dragOffset = 0
                self.currentIndex += 1
            }
        } else {
            withAnimation {
                self.dragOffset = 0
            }
        }
    }
    
    var backButton: some View {
        Button(action: {
            withAnimation {
                self.currentIndex -= 1
            }
        }) {
            Image(systemName: "arrow.left")
                .imageScale(.large)
                .padding()
                .accentColor(.black)
        }
    }
    
    var setupView: some View {
        VStack(alignment: .center, spacing: 0) {
            ZStack {
                GeometryReader { geometry in
                    VStack {
                        Spacer()
                        Text("ProofMode activates multiple sensors on your phone to generate proof.")
                            .font(Font.custom(Font.titleFont, size: 21))
                            .foregroundColor(.black)
                            .multilineTextAlignment(.center)
                            .padding(EdgeInsets(top: 0, leading: 50, bottom: 20, trailing: 50))
                        
                        Text("Learn about what ProofMode captures in our Data Legend.")
                            .font(Font.custom(Font.bodyFont, size: 13))
                            .foregroundColor(.black)
                            .multilineTextAlignment(.center)
                            .padding(EdgeInsets(top: 0, leading: 50, bottom: 20, trailing: 50))
                        
                        Spacer()
                        VStack(alignment: .center, spacing: 8) {
                            Image(systemName: "heart.fill").resizable()
                                .resizable()
                                .foregroundColor(Color("colorAccent"))
                                .frame(width: 18, height: 16, alignment: .center)
                            Text("WE CARE ABOUT YOUR PRIVACY")
                                .font(Font.custom(Font.titleFont, size: 13).smallCaps())
                                .fontWeight(.bold)
                                .foregroundColor(.black)
                            Text("ProofMode captures your phone’s identity by default. If you’re not comfortable with this, go to settings to turn it off.")
                                .font(Font.custom(Font.titleFont, size: 13))
                                .fontWeight(.regular)
                                .multilineTextAlignment(.center)
                                .foregroundColor(.black)
                        }.padding()
                        
                        Spacer()
                        Spacer()
                        VStack(alignment: .center, spacing: 15) {
                            Button(action: {
                                withAnimation {
                                    self.checkPermissions()
                                }
                            }) {
                                Text("Continue")
                                    .font(Font.system(size: 13).smallCaps())
                                    .foregroundColor(.white)
                                    .padding(8)
                            }.padding().frame(height: 30).background(Color.black).cornerRadius(15)
                            
                            NavigationLink(destination: SettingsView()) {
                                Text("Settings")
                                    .font(Font.system(size: 13).smallCaps())
                                    .foregroundColor(.black)
                                    .padding(8)
                            }.padding().frame(height: 30).background(RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.black, lineWidth: 2))
                        }
                        Spacer()
                    }
                }
            }
        }
    }
    
    func checkPermissions() {
        // Location
        //
        if Settings.shared.optionLocation {
            // Have permission?
            let cll = CLLocationManager()
            let authStatus = cll.authorizationStatus
            switch authStatus {
            case .notDetermined:
                LocationManager.shared.getPermission { status in
                    if ![.authorizedAlways, .authorizedWhenInUse].contains(status) {
                        Settings.shared.optionLocation = false
                    }
                    checkPermissions()
                }
                return
            case .denied, .restricted:
                Settings.shared.optionLocation = false
                break
            default: break
            }
        }
        
        // Phone ID
        if Settings.shared.optionPhone {
            let authStatus = ATTrackingManager.trackingAuthorizationStatus
            switch authStatus {
            case .notDetermined:
                ATTrackingManager.requestTrackingAuthorization { status in
                    DispatchQueue.main.async {
                        if status != .authorized {
                            Settings.shared.optionPhone = false
                        }
                        checkPermissions()
                    }
                }
                return
            case .denied, .restricted:
                Settings.shared.optionPhone = false
                break
            default:
                break
            }
        }

        
        // Done, we have all we need
        DispatchQueue.main.async {
            Settings.shared.onboarded = true
        }
    }
}

struct WelcomeView: View {
    @Binding var currentIndex: Int
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                VStack {
                    Spacer()
                    Text("ProofMode")
                        .font(Font.custom(Font.titleFont, size: 13).smallCaps())
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 0, leading: 50, bottom: 20, trailing: 50))
                    
                    Text("A picture may be worth a thousand words, but proof that it’s real is priceless.")
                        .font(Font.custom(Font.titleFont, size: 21))
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 0, leading: 50, bottom: 20, trailing: 50))
                    
                    Text("ProofMode captures information that makes your media more trustworthy.")
                        .font(Font.custom(Font.bodyFont, size: 13))
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                        .padding(EdgeInsets(top: 0, leading: 50, bottom: 20, trailing: 50))
                    
                    NavigationLink(destination: DataLegendView()) {
                        Text("Learn More")
                            .font(Font.system(size: 13).smallCaps())
                            .foregroundColor(.white)
                            .padding(8)
                    }
                }
                .frame(width: geometry.size.width, height: geometry.size.height * 0.4)
                
                Spacer()
                Spacer()
                Button(action: {
                    withAnimation {
                        self.currentIndex = 1
                    }
                }) {
                    Text("Get Started")
                        .font(Font.system(size: 13).smallCaps())
                        .foregroundColor(.white)
                        .padding(8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.white, lineWidth: 2)
                                .frame(height: 30)
                        )
                }.frame(height: 30)
                Spacer()
            }
            .background(                            Image("img_home-off")
                .resizable()
                .edgesIgnoringSafeArea(.top)
                .aspectRatio(UIImage(named: "img_home-off")!.size, contentMode: .fill)
            )
            //.ignoresSafeArea()
            //            .background(Color.red.ignoresSafeArea())
            
        }
        //.ignoresSafeArea()
    }
}

struct PageView: View {
    var page: Page
    
    @Binding var currentIndex: Int
    
    var body: some View {
        VStack(alignment: .center, spacing: 15) {
            Text(self.page.title)
                .font(.custom(Font.titleFont, size: 21))
                .fontWeight(.bold)
                .foregroundColor(Color("colorDarkGray"))
                .multilineTextAlignment(.center)
                .padding()
            
            Text(self.page.content)
                .font(.custom(Font.bodyFont, size: 13))
                .fontWeight(.regular)
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .padding()
            
            ZStack {
                Image(self.page.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .clipped()
            }
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity,
                   alignment: .topLeading
            )
        }
    }
}

struct PageIndicator: View {
    @Binding var isSelected: Bool
    let action: () -> Void
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            ZStack {
                Circle().fill(self.isSelected ? Color("colorPrimary") : .white)
                    .frame(width: 9, height: 9)
                Circle().stroke(Color("colorPrimary"))
                    .frame(width: 9, height: 9)
            }
        }
    }
}

struct HowItWorksView_Previews: PreviewProvider {
    @State static var currentIndex = 2
    static var previews: some View {
        Group {
            HowItWorksView(onboarding: true)
                .previewDevice("iPhone 8")
                .previewInterfaceOrientation(.portrait)
            
            WelcomeView(currentIndex: $currentIndex)
        }
    }
}
