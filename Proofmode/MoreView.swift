//
//  MoreView.swift
//  Proofmode
//
//  Created by N-Pex on 2023-04-14.
//

import SwiftUI
import LibProofMode

fileprivate enum DismissAction {
    case nothing
    case importMedia
    case sharePublicKey(key: String)
}

struct MoreView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.onSharePublicKey) var onSharePublicKey
    @Environment(\.onImportMedia) var onImportMedia
    @Environment(\.dismiss) private var dismiss
    @Environment(\.onNavigateToScreen) private var onNavigateToScreen
    
    //@Binding var navigationPath: [NavigationScreen]
    @State private var dismissAction: DismissAction = .nothing
    @State private var showSettings: Bool = false
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            Text("More")
                .font(Font.largeTitle)
                .fontWeight(.bold)
                .multilineTextAlignment(.leading)
            
            Button(action: {
                self.dismissAction = .importMedia
                dismiss()
            }) {
                HStack {
                    Image(systemName: "square.and.arrow.down")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("Import media")
                        .font(.custom(Font.bodyFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
                .padding(.top, 8)
            }
            
            Button(action: {
                withAnimation {
                    if let publicKeyString = Proof.shared.getPublicKeyString() {
                        self.dismissAction = .sharePublicKey(key: publicKeyString)
                        dismiss()
                    }
                }
            }) {
                HStack {
                    Image(systemName: "key")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("Share public key...")
                        .font(.custom(Font.bodyFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
                .padding(.top, 8)
            }
            
            Button(action: {
                onNavigateToScreen?(.settings)
            }) {
                HStack {
                    Image("ic_settings")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("Settings")
                        .font(.custom(Font.bodyFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
                .padding(.top, 8)
            }
            .accessibilityIdentifier("more_settingsButton")
            
            Button(action: { onNavigateToScreen?(.howItWorks) }) {
                HStack {
                    Image("ic_how-it-works")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("How It Works")
                        .font(.custom(Font.bodyFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
                .padding(.top, 48)
            }
            
            Button(action: { onNavigateToScreen?(.dataLegend) }) {
                HStack {
                    Image("ic_data-legend")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("Data Legend")
                        .font(.custom(Font.bodyFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
                .padding(.top, 8)
            }
            
            Button(action: { onNavigateToScreen?(.digitalSignatures) }) {
                HStack {
                    Image("ic_digi-sig")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("Digital Signatures")
                        .font(.custom(Font.bodyFont, size: 13))
                        .fontWeight(.regular)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
                .padding(.top, 8)
            }
            
            Spacer()
        }
        .padding(10)
        .frame(maxWidth: .infinity, alignment: .leading)
        .background(Color.white)
        .customBackButton(title: "Back")
        .onDisappear {
            switch self.dismissAction {
            case .importMedia:
                self.onImportMedia?()
            case .sharePublicKey(let key):
                self.onSharePublicKey?(key)
            default: break
            }
            self.dismissAction = .nothing
        }
    }
}

struct MoreView_Previews: PreviewProvider {
    @State static var navigationPath: [NavigationScreen] = [.more]
    static var previews: some View {
        MoreView().environment(\.onNavigateToScreen, { screen in
        })
    }
}
