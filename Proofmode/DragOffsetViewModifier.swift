//
//  DragOffsetViewModifier.swift
//  Proofmode
//
//  Created by N-Pex on 2023-04-07.
//

import SwiftUI

// A DragGesture modifier that can handle cancellations, that can occur e.g. if view contains a scrollview.
// In that case onEnded was never called on "normal" DragGestures. We make sure to do that.
//
// Modified the code from https://developer.apple.com/forums/thread/123034

struct DragGestureViewModifier: ViewModifier {
    @GestureState private var isDragging: Bool = false
    @State var gestureState: GestureStatus = .idle
    @State var highPriority: Bool = false
    
    var onStart: (() -> Void)?
    var onUpdate: ((DragGesture.Value) -> Void)?
    var onEnd: ((DragGesture.Value?) -> Void)?

    func body(content: Content) -> some View {
        content
            .if (highPriority) { view in
                view.simultaneousGesture(DragGesture(minimumDistance: 1, coordinateSpace: .global)
                    .updating($isDragging) { _, isDragging, _ in
                        isDragging = true
                    }
                    .onChanged(onDragChange(_:))
                    .onEnded(onDragEnded(_:))
                )
            }
            .if (!highPriority) { view in
                view.gesture(DragGesture(minimumDistance: 1, coordinateSpace: .global)
                    .updating($isDragging) { _, isDragging, _ in
                        isDragging = true
                    }
                    .onChanged(onDragChange(_:))
                    .onEnded(onDragEnded(_:))
                )
            }
            .onChange(of: gestureState) { state in
                guard state == .started else { return }
                gestureState = .active
            }
            .onChange(of: isDragging) { value in
                if value, gestureState != .started {
                    gestureState = .started
                    onStart?()
                } else if !value, gestureState != .ended {
                    gestureState = .cancelled
                    onEnd?(nil)
                }
            }
    }

    func onDragChange(_ value: DragGesture.Value) {
        guard gestureState == .started || gestureState == .active else { return }
        onUpdate?(value)
    }

    func onDragEnded(_ value: DragGesture.Value) {
        gestureState = .ended
        onEnd?(value)
    }

    enum GestureStatus: Equatable {
        case idle
        case started
        case active
        case ended
        case cancelled
    }
}
