//
//  EnvironmentValues+Proofmode.swift
//  Proofmode
//
//  Created by N-Pex on 2023-03-31.
//

import SwiftUI


private struct OnAssetTappedEnvironmentKey: EnvironmentKey {
    static let defaultValue: ((CameraItem) -> Void)? = nil
}

private struct OnAssetLongTappedEnvironmentKey: EnvironmentKey {
    static let defaultValue: ((CameraItem) -> Void)? = nil
}

private struct SelectedAssetsEnvironmentKey: EnvironmentKey {
    static let defaultValue: [CameraItem] = []
}

private struct ThumbnailManagerEnvironmentKey: EnvironmentKey {
    static let defaultValue: ThumbnailManager = ThumbnailManager()
}

private struct OnShareItemEnvironmentKey: EnvironmentKey {
    static let defaultValue: ((Any, _ forAssets: [CameraItem]) -> Void)? = nil
}

private struct OnSharePublicKeyEnvironmentKey: EnvironmentKey {
    static let defaultValue: ((String) -> Void)? = nil
}

private struct OnBundleItemsEnvironmentKey: EnvironmentKey {
    static let defaultValue: (([CameraItem]) -> Void)? = nil
}
private struct OnC2PAShareItemsEnvironmentKey: EnvironmentKey {
    static let defaultValue: (([CameraItem]) -> Void)? = nil
}

private struct OnImportMediaEnvironmentKey: EnvironmentKey {
    static let defaultValue: (() -> Void)? = nil
}

private struct OnNavigateToScreenEnvironmentKey: EnvironmentKey {
    static let defaultValue: ((NavigationScreen) -> Void)? = nil
}

extension EnvironmentValues {
    var selectedAssets: [CameraItem] {
        get { self[SelectedAssetsEnvironmentKey.self] }
        set { self[SelectedAssetsEnvironmentKey.self] = newValue }
    }
    
    var onAssetTapped: ((CameraItem) -> Void)? {
        get { self[OnAssetTappedEnvironmentKey.self] }
        set { self[OnAssetTappedEnvironmentKey.self] = newValue }
    }
    
    var onAssetLongTapped: ((CameraItem) -> Void)? {
        get { self[OnAssetLongTappedEnvironmentKey.self] }
        set { self[OnAssetLongTappedEnvironmentKey.self] = newValue }
    }
    
    var thumbnailManager: ThumbnailManager {
        get { self[ThumbnailManagerEnvironmentKey.self] }
        set { self[ThumbnailManagerEnvironmentKey.self] = newValue }
    }

    var onShareItem: ((Any, _ forAssets: [CameraItem]) -> Void)? {
        get { self[OnShareItemEnvironmentKey.self] }
        set { self[OnShareItemEnvironmentKey.self] = newValue }
    }

    var onSharePublicKey: ((String) -> Void)? {
        get { self[OnSharePublicKeyEnvironmentKey.self] }
        set { self[OnSharePublicKeyEnvironmentKey.self] = newValue }
    }

    var onBundleItems: (([CameraItem]) -> Void)? {
        get { self[OnBundleItemsEnvironmentKey.self] }
        set { self[OnBundleItemsEnvironmentKey.self] = newValue }
    }

    var onC2PAShareItems: (([CameraItem]) -> Void)? {
        get { self[OnC2PAShareItemsEnvironmentKey.self] }
        set { self[OnC2PAShareItemsEnvironmentKey.self] = newValue }
    }

    var onImportMedia: (() -> Void)? {
        get { self[OnImportMediaEnvironmentKey.self] }
        set { self[OnImportMediaEnvironmentKey.self] = newValue }
    }
    
    var onNavigateToScreen: ((NavigationScreen) -> Void)? {
        get { self[OnNavigateToScreenEnvironmentKey.self] }
        set { self[OnNavigateToScreenEnvironmentKey.self] = newValue }
    }
}
