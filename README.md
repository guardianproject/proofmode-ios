# Port of ProofMode to iOS

## Manually Building 

Note: to build, you need to have rust installed and first go into the /Rust folder and run build.sh, to build all required rust code bindings.

## Automated Builds

[![Build Status](https://app.bitrise.io/app/4b2a5da418a79933/status.svg?token=4ud_non1l6Hoo8RZIbmXrg&branch=master)](https://app.bitrise.io/app/4b2a5da418a79933)

Changes to `main` are automatically built on bitrise and the resulting .ipa can be installed on select pre-configured devices. Contributors, please file an issue with your device ID to be added to this list.

Tests will automatically be run for pull requests against `main` from within the project. Because of Bitrise's secret protection scheme, .ipa files will not be produced for PRs.

Separately, [a public testflight beta is available via proofmode.org](https://proofmode.org/install)
